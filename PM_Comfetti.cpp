#include "Particle.cpp"

#define HUE_DIVIDER 60.f
#define FMOD_DIVIDER 2
#define HUE_RANGE_1 60
#define HUE_RANGE_2 120
#define HUE_RANGE_3 180
#define HUE_RANGE_4 240
#define HUE_RANGE_5 300
#define HUE_RANGE_6 360
#define HSV_TO_RGB_MULTIPLIER 255

/*
Confetti particle class
*/
class P_Confetti : public Particle
{
private:
    std::vector<sf::Color> Colors;

    sf::Color HSVToRGB(const int H) const;
    void makeShape(part &newPart);
    void updateMiddleware(part &updatePart, const int delta_time);

public:
    P_Confetti(const float x, const float y, const int particleLineLen, const int lifeSpan, const int partsFrequency, const int momXMin, const int momXMax, const int momYMin, const int momYMax, const int vertGravity, const int horGravity);
};

P_Confetti::P_Confetti(const float x, const float y, const int particleLineLen = CONFETTI_DEFUALT_LINE_LEN, const int lifeSpan = CONFETTI_DEFAULT_LIFE_SPAN, const int partsFrequency = CONFETTI_DEFAULT_FREQUENCY, const int momXMin = CONFETTI_DEFAULT_MOMENTUM_X_MIN, const int momXMax = CONFETTI_DEFAULT_MOMENTUM_X_MAX, const int momYMin = CONFETTI_DEFAULT_MOMENTUM_Y_MIN, const int momYMax = CONFETTI_DEFAULT_MOMENTUM_Y_MAX, const int vertGravity = CONFETTI_DEFUALT_VERTICLE_GRAVITY, const int horGravity = CONFETTI_DEFUALT_HORIZONTAL_GRAVITY) : Particle(x, y, particleLineLen, lifeSpan, partsFrequency, momXMin, momXMax, momYMin, momYMax, vertGravity, horGravity), Colors({{sf::Color(255, 0, 0)}, {sf::Color(255, 0, 255)}, {sf::Color(0, 255, 0)}})
{
}

// A function to make the shape of the confettis' particle
void P_Confetti::makeShape(part &newPart)
{
    float width = rand() % (CONFETTI_MAX_WIDTH - CONFETTI_MIN_WIDTH) + CONFETTI_MIN_WIDTH, height = width / CONFETTI_WIDTH_DIVIDE;
    int colotIndex = rand() % Colors.size();

    newPart.shape = new sf::ConvexShape(POINTS_IN_CONFETTI_SHAPE);
    (*(sf::ConvexShape *)newPart.shape).setPoint(0, sf::Vector2f(0, 0));
    (*(sf::ConvexShape *)newPart.shape).setPoint(1, sf::Vector2f(width / WIDTH_DIVIDER, -height / HEIGHT_DIVIDER));
    (*(sf::ConvexShape *)newPart.shape).setPoint(2, sf::Vector2f(width / WIDTH_DIVIDER * WIDTH_FAR_MULTIPLIER, -height / HEIGHT_DIVIDER));
    (*(sf::ConvexShape *)newPart.shape).setPoint(3, sf::Vector2f(width, 0));
    (*(sf::ConvexShape *)newPart.shape).setPoint(4, sf::Vector2f(width, height));
    (*(sf::ConvexShape *)newPart.shape).setPoint(5, sf::Vector2f(width / WIDTH_DIVIDER * WIDTH_FAR_MULTIPLIER, height - height / HEIGHT_DIVIDER));
    (*(sf::ConvexShape *)newPart.shape).setPoint(6, sf::Vector2f(width / WIDTH_DIVIDER, height - height / HEIGHT_DIVIDER));
    (*(sf::ConvexShape *)newPart.shape).setPoint(7, sf::Vector2f(0, height));

    //newPart.shape = new sf::RectangleShape(sf::Vector2f(width, height));
    newPart.shape->setFillColor(HSVToRGB(rand() % HUE_RANGE_6));
    newPart.shape->setOutlineThickness(CONFETTI_OUTLINE_THICKNESS);
    newPart.shape->setOutlineColor(sf::Color(CONFETTI_OUTLINE_COLOR));
    newPart.shape->setPosition(position.x + rand() % lineLen - width / HALF, position.y - height / HALF);
}

// A function to update properties specific to the confettis' particle before drawing
void P_Confetti::updateMiddleware(part &updatePart, const int delta_time)
{
    // updating the particle's edges to reflect the particle falling
    sf::Vector2f sizes = (*(sf::ConvexShape *)updatePart.shape).getPoint(POINTS_IN_CONFETTI_SHAPE / HALF);
    int quarter_width = sizes.x / WIDTH_DIVIDER, height_change = std::max(std::min(updatePart.momentum_y * PARTS_MOMENTUM_MULTIPLIER, sizes.y / HEIGHT_DIVIDER), -sizes.y / HEIGHT_DIVIDER);

    (*(sf::ConvexShape *)updatePart.shape).setPoint(1, sf::Vector2f(quarter_width, -height_change));
    (*(sf::ConvexShape *)updatePart.shape).setPoint(2, sf::Vector2f(quarter_width * WIDTH_FAR_MULTIPLIER, -height_change));
    (*(sf::ConvexShape *)updatePart.shape).setPoint(5, sf::Vector2f(quarter_width * WIDTH_FAR_MULTIPLIER, sizes.y - height_change));
    (*(sf::ConvexShape *)updatePart.shape).setPoint(6, sf::Vector2f(quarter_width, sizes.y - height_change));
}

// A function to return the RGB color made from a given H (S and V are always 100)
// Credit to Amit Raja Kalidindi: https://www.codespeedy.com/hsv-to-rgb-in-cpp/
sf::Color P_Confetti::HSVToRGB(const int H) const
{
    float X = (1 - fabs(fmod(H / HUE_DIVIDER, FMOD_DIVIDER) - 1));
    float r, g, b;

    // For different ranges of the Hue
    if (H >= 0 && H < HUE_RANGE_1)
    {
        r = 1, g = X, b = 0;
    }
    else if (H >= HUE_RANGE_1 && H < HUE_RANGE_2)
    {
        r = X, g = 1, b = 0;
    }
    else if (H >= HUE_RANGE_2 && H < HUE_RANGE_3)
    {
        r = 0, g = 1, b = X;
    }
    else if (H >= HUE_RANGE_3 && H < HUE_RANGE_4)
    {
        r = 0, g = X, b = 1;
    }
    else if (H >= HUE_RANGE_4 && H < HUE_RANGE_5)
    {
        r = X, g = 0, b = 1;
    }
    else
    {
        r = 1, g = 0, b = X;
    }

    return sf::Color(r * HSV_TO_RGB_MULTIPLIER, g * HSV_TO_RGB_MULTIPLIER, b * HSV_TO_RGB_MULTIPLIER);
}