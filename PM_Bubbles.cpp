#include "Particle.cpp"

/*
Bubbles particle class
*/
class P_Bubbles : public Particle
{
private:
    void makeShape(part &newPart);
    void updateMiddleware(part &updatePart, const int delta_time);

public:
    P_Bubbles(const float x, const float y, const int particleLineLen, const int lifeSpan, const int partsFrequency, const int momXMin, const int momXMax, const int momYMin, const int momYMax, const int vertGravity, const int horGravity);
};

P_Bubbles::P_Bubbles(const float x, const float y, const int particleLineLen = BUBBLES_DEFUALT_LINE_LEN, const int lifeSpan = BUBBLES_DEFAULT_LIFE_SPAN, const int partsFrequency = BUBBLES_DEFAULT_FREQUENCY, const int momXMin = BUBBLES_DEFAULT_MOMENTUM_X_MIN, const int momXMax = BUBBLES_DEFAULT_MOMENTUM_X_MAX, const int momYMin = BUBBLES_DEFAULT_MOMENTUM_Y_MIN, const int momYMax = BUBBLES_DEFAULT_MOMENTUM_Y_MAX, const int vertGravity = BUBBLES_DEFUALT_VERTICLE_GRAVITY, const int horGravity = BUBBLES_DEFUALT_HORIZONTAL_GRAVITY) : Particle(x, y, particleLineLen, lifeSpan, partsFrequency, momXMin, momXMax, momYMin, momYMax, vertGravity, horGravity)
{
}

// A function to make the shape of the bubbles' particle
void P_Bubbles::makeShape(part &newPart)
{
    float r = rand() % BUBBLES_MAX_RADIUS_ADD + BUBBLES_MIN_RADIUS;
    newPart.shape = new sf::CircleShape(r);
    newPart.shape->setFillColor(sf::Color{BUBBLES_COLOR});
    newPart.shape->setOutlineThickness(BUBBLES_OUTLINE_THICKNESS);
    newPart.shape->setOutlineColor(sf::Color(BUBBLES_OUTLINE_COLOR));
    newPart.shape->setPosition(position.x + rand() % lineLen - r, position.y - r);
}

// A function to update properties specific to the bubbles' particle before drawing
void P_Bubbles::updateMiddleware(part &updatePart, const int delta_time)
{
}