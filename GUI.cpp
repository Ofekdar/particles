#include "PM_Bubbles.cpp"
#include "PM_Sparks.cpp"
#include "PM_Comfetti.cpp"

#define DEFAULT_OFFSET 40
#define SCROLLER_MULTIPLIER 20
#define BUTTONS_OUTLINE_THICKNESS 2

#define BACKGROUND_COLOR 200, 200, 255
#define BUTTONS_COLOR 255, 255, 204
#define BUTTONS_HOVER_COLOR 250, 215, 160
#define BUTTONS_SELECTED_COLOR 255, 178, 102
#define MAIN_BUTTONS_HOVER_COLOR 204, 229, 255
#define MAIN_BUTTONS_SELECTED_COLOR 153, 204, 255

#define MAIN_BUTTONS_SIZES 180, 50
#define MAIN_BUTTONS_FONT_SIZE 25
#define MAIN_BUTTONS_MARGIN 15
#define MAIN_BUTTONS_X 40

#define PARAMS_BUTTONS_HEIGHT 65
#define PARAMS_BUTTONS_SIZES 250, PARAMS_BUTTONS_HEIGHT
#define SPACE_BETWEEN_PARAMS_BUTTONS 30
#define PARAMS_BUTTONS_FILLER_SIZES 250, SPACE_BETWEEN_PARAMS_BUTTONS - BUTTONS_OUTLINE_THICKNESS
#define PARAMS_BUTTONS_X 260
#define PARAMS_BUTTONS_FONT_SIZE 15

#define PARTICLE_WINDOW_MARGIN 40
#define PARTICLE_WINDOW_COLOR 70, 70, 70

#define NUMBERS_ASCII_START 48
#define NUMBERS_ASCII_END 58
#define BACKSPACE_ASCII_VALUE 8
#define MINUS_ASCII_VALUE 45
#define CHARS_IN_HIGHEST 7
#define CHARS_IN_LOWEST 6

typedef struct ParamValues
{
    int currentValue;
    int minValue;
    int maxValue;
} ParamValues;

class GUI
{
private:
    sf::RenderWindow gui;
    int offset;

    std::string selectedOption;
    std::string hoveredOption;
    Particle *preview;
    bool clearField;

    const std::map<int, std::string> types;
    std::vector<std::string> mainButtonsStrings;
    std::vector<std::string> paramsStrings;
    std::map<std::string, ParamValues> params;

    std::vector<sf::RectangleShape> basicRects;
    sf::RectangleShape previewWindow;

    void renderAll();
    void drawButtons();

    sf::Vector2f getTextPos(sf::FloatRect boxRect, sf::FloatRect textRect);
    bool getColliding(sf::FloatRect box, sf::Vector2i pos);
    void checkMouseBoxes(const sf::Vector2i newPos);
    bool checkValueMinMax();
    void restoreParticleDefaults();

    bool updateParam(const int keyVal);
    void updateParticle();

public:
    GUI();
    ~GUI();

    bool tick(const int deltaTime);
};

GUI::GUI() : gui(sf::VideoMode(MENU_WIDTH, MENU_HEIGHT), "Menu", sf::Style::Default),
             types{{0, "Bubbles"}, {1, "Sparks"}, {2, "Confetti"}},
             mainButtonsStrings{"ADD", "SWITCH"},
             paramsStrings{"Type", "Frequency", "Life Span", "Line Length", "Momentum X Lowest", "Momentum X Highest", "Momentum Y Lowest", "Momentum Y Highest", "Verticle Gravity", "Horizontal Gravity"},
             params{{"Type", ParamValues{DEFAULT_TYPE, MIN_TYPE, MAX_TYPE}}, {"Frequency", ParamValues{BUBBLES_DEFAULT_FREQUENCY, MIN_FREQUENCY, MAX_FREQUENCY}}, {"Life Span", ParamValues{BUBBLES_DEFAULT_LIFE_SPAN, MIN_LIFE_SPAN, MAX_LIFE_SPAN}}, {"Line Length", ParamValues{BUBBLES_DEFUALT_LINE_LEN, MIN_LINE_LEN, MAX_LINE_LEN}}, {"Momentum X Lowest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_X_MIN, MIN_MOMENTUM_X, BUBBLES_DEFAULT_MOMENTUM_X_MAX}}, {"Momentum X Highest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_X_MAX, BUBBLES_DEFAULT_MOMENTUM_X_MIN, MAX_MOMENTUM_X}}, {"Momentum Y Lowest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_Y_MIN, MIN_MOMENTUM_Y, BUBBLES_DEFAULT_MOMENTUM_Y_MAX}}, {"Momentum Y Highest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_Y_MAX, BUBBLES_DEFAULT_MOMENTUM_Y_MIN, MAX_MOMENTUM_Y}}, {"Verticle Gravity", ParamValues{BUBBLES_DEFUALT_VERTICLE_GRAVITY, MIN_VERTICLE_GRAVITY, MAX_VERTICLE_GRAVITY}}, {"Horizontal Gravity", ParamValues{BUBBLES_DEFUALT_HORIZONTAL_GRAVITY, MIN_HORIZONTAL_GRAVITY, MAX_HORIZONTAL_GRAVITY}}},
             offset(DEFAULT_OFFSET),
             hoveredOption(""),
             preview(new P_Bubbles(PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN + (gui.getSize().x - PARTICLE_WINDOW_MARGIN * DOUBLE - PARAMS_BUTTONS_X - sf::Vector2i(PARAMS_BUTTONS_SIZES).x) / HALF, PARTICLE_WINDOW_MARGIN + gui.getSize().y / HALF)),
             clearField(true)
{
    previewWindow = sf::RectangleShape(sf::Vector2f(gui.getSize().x - (PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN * DOUBLE), gui.getSize().y - PARTICLE_WINDOW_MARGIN * DOUBLE));
    previewWindow.setOutlineThickness(-BUTTONS_OUTLINE_THICKNESS);
    previewWindow.setFillColor(sf::Color(PARTICLE_WINDOW_COLOR));
    previewWindow.setPosition(PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN, PARTICLE_WINDOW_MARGIN);

    sf::RectangleShape rect(sf::Vector2f(sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARAMS_BUTTONS_X + PARTICLE_WINDOW_MARGIN, gui.getSize().y));
    rect.setFillColor(sf::Color(BACKGROUND_COLOR));
    basicRects.push_back(rect);

    rect.setSize(sf::Vector2f(gui.getSize().x, PARTICLE_WINDOW_MARGIN));
    basicRects.push_back(rect);

    rect.setPosition(0, gui.getSize().y - PARTICLE_WINDOW_MARGIN);
    basicRects.push_back(rect);

    rect.setSize(sf::Vector2f(PARTICLE_WINDOW_MARGIN, gui.getSize().y));
    rect.setPosition(gui.getSize().x - PARTICLE_WINDOW_MARGIN, 0);
    basicRects.push_back(rect);

    selectedOption = paramsStrings[0];
    renderAll();
}

GUI::~GUI()
{
    gui.close();
    delete preview;
}

// A function to run every tick of loop
// Listens to events and handles them
bool GUI::tick(const int deltaTime)
{
    sf::Event event;
    int lowerEnd;
    while (gui.pollEvent(event))
    {
        switch (event.type)
        {
        // if user pressed the 'X' button, return true to main function
        case sf::Event::Closed:
            return true;

        // if user scrolled the mouse wheel, scroll the params' boxes
        case sf::Event::MouseWheelMoved:
            lowerEnd = DEFAULT_OFFSET - (PARAMS_BUTTONS_HEIGHT + SPACE_BETWEEN_PARAMS_BUTTONS) * (paramsStrings.size() + 1) + gui.getSize().y;
            if (offset + event.mouseWheel.delta * SCROLLER_MULTIPLIER <= DEFAULT_OFFSET && offset + event.mouseWheel.delta * SCROLLER_MULTIPLIER >= lowerEnd)
            {
                checkMouseBoxes(sf::Mouse::getPosition(gui));
                offset += event.mouseWheel.delta * SCROLLER_MULTIPLIER;
            }
            break;

        // if the mouse moved, check for collisions with the params' boxes
        case sf::Event::MouseMoved:
            checkMouseBoxes(sf::Mouse::getPosition(gui));
            break;

        // if the mouse button was pressed, change the selected param
        case sf::Event::MouseButtonPressed:
            if (hoveredOption != "")
            {
                clearField = true;
                selectedOption = hoveredOption;
            }
            break;

        // for when a key/backspace is pressed, hcange the value of the selected param
        case sf::Event::TextEntered:
            if (updateParam(event.text.unicode))
            {
                updateParticle();
            }
            break;

        default:
            break;
        }
    }
    preview->move(deltaTime);
    renderAll();
    return false;
}

// A function to render everything there is to render on the gui
void GUI::renderAll()
{
    // rendering the preview particle's window
    gui.draw(previewWindow);

    // rendering the preview particle
    preview->blit(gui);

    // rendering the background rects
    for (sf::RectangleShape rect : basicRects)
    {
        gui.draw(rect);
    }

    // rendering all the buttons
    drawButtons();

    // updating the display
    gui.display();
}

// A function to get the position of text that's inside a rectangle
sf::Vector2f GUI::getTextPos(sf::FloatRect boxRect, sf::FloatRect textRect)
{
    return sf::Vector2f(boxRect.left + ((boxRect.width - textRect.width) / HALF), boxRect.top + ((boxRect.height - textRect.height) / HALF));
}

// A function to check if a position is inside a box
bool GUI::getColliding(sf::FloatRect box, sf::Vector2i pos)
{
    return pos.x > box.left && pos.x < box.left + box.width && pos.y > box.top && pos.y < box.top + box.height;
}

// A function to draw the params' and the mains' buttons
void GUI::drawButtons()
{
    // some starting variables
    int i = 0;
    sf::RectangleShape rect(sf::Vector2f(MAIN_BUTTONS_SIZES));
    sf::RectangleShape between(sf::Vector2f(PARAMS_BUTTONS_FILLER_SIZES));
    sf::Font font;

    // some basic parameters
    font.loadFromFile("pixel_font.TTF");
    rect.setOutlineThickness(BUTTONS_OUTLINE_THICKNESS);
    between.setFillColor(sf::Color(BACKGROUND_COLOR));
    between.setPosition(sf::Vector2f(PARAMS_BUTTONS_X, 0));
    gui.draw(between);

    // the "ADD/DELETE" rect
    rect.setPosition(sf::Vector2f(MAIN_BUTTONS_X, gui.getSize().y / HALF - sf::Vector2i(MAIN_BUTTONS_SIZES).y - MAIN_BUTTONS_MARGIN));
    rect.setFillColor(selectedOption == mainButtonsStrings[0] ? sf::Color(MAIN_BUTTONS_SELECTED_COLOR) : hoveredOption == mainButtonsStrings[0] ? sf::Color(MAIN_BUTTONS_HOVER_COLOR)
                                                                                                                                                : sf::Color(BUTTONS_COLOR));
    gui.draw(rect);

    // the "ADD/DELETE" label
    sf::Text text(mainButtonsStrings[0], font, MAIN_BUTTONS_FONT_SIZE);
    text.setPosition(getTextPos(rect.getGlobalBounds(), text.getGlobalBounds()));
    text.setFillColor(sf::Color::Black);
    gui.draw(text);

    // the "SWITCH" rect
    rect.setPosition(sf::Vector2f(MAIN_BUTTONS_X, gui.getSize().y / HALF + MAIN_BUTTONS_MARGIN));
    rect.setFillColor(selectedOption == mainButtonsStrings[1] ? sf::Color(MAIN_BUTTONS_SELECTED_COLOR) : hoveredOption == mainButtonsStrings[1] ? sf::Color(MAIN_BUTTONS_HOVER_COLOR)
                                                                                                                                                : sf::Color(BUTTONS_COLOR));
    gui.draw(rect);

    // the "SWITCH" label
    text.setString(mainButtonsStrings[1]);
    text.setPosition(getTextPos(rect.getGlobalBounds(), text.getGlobalBounds()));
    gui.draw(text);

    rect.setSize(sf::Vector2f(PARAMS_BUTTONS_SIZES));

    // for every parameter
    for (i = 0; i < paramsStrings.size(); i++)
    {
        // drawing the rect and the background that's after
        rect.setPosition(sf::Vector2f(PARAMS_BUTTONS_X, offset + (PARAMS_BUTTONS_HEIGHT + SPACE_BETWEEN_PARAMS_BUTTONS) * i));
        between.setPosition(sf::Vector2f(PARAMS_BUTTONS_X, offset + (PARAMS_BUTTONS_HEIGHT + SPACE_BETWEEN_PARAMS_BUTTONS) * (i + 1) - SPACE_BETWEEN_PARAMS_BUTTONS + BUTTONS_OUTLINE_THICKNESS));
        rect.setFillColor(selectedOption == paramsStrings[i] ? sf::Color(BUTTONS_SELECTED_COLOR) : hoveredOption == paramsStrings[i] ? sf::Color(BUTTONS_HOVER_COLOR)
                                                                                                                                     : sf::Color(BUTTONS_COLOR));
        gui.draw(rect);
        gui.draw(between);

        // adding the parameter's label
        sf::Text text(paramsStrings[i] + ":\n", font, PARAMS_BUTTONS_FONT_SIZE);
        text.setFillColor(sf::Color::Black);
        text.setPosition(getTextPos(rect.getGlobalBounds(), text.getGlobalBounds()));
        gui.draw(text);

        // adding the parameter's value
        text.setString("\n" + (i ? std::to_string(params[paramsStrings[i]].currentValue) : std::to_string(params[paramsStrings[i]].currentValue) + " - " + types.at(params[paramsStrings[i]].currentValue - 1)));
        text.setPosition(getTextPos(rect.getGlobalBounds(), text.getGlobalBounds()));
        gui.draw(text);
    }
}

// A function to check when the mouse moves if he's on a button or not
void GUI::checkMouseBoxes(const sf::Vector2i newPos)
{
    int i = 0;
    bool found = false;

    // if the mouse is inside the main buttons' bounderies and colliding with the (non selected) main button, hover it
    if (newPos.x > MAIN_BUTTONS_X && newPos.x < MAIN_BUTTONS_X + sf::Vector2i(MAIN_BUTTONS_SIZES).x &&
        (getColliding(sf::FloatRect(MAIN_BUTTONS_X, gui.getSize().y / HALF + MAIN_BUTTONS_MARGIN, MAIN_BUTTONS_SIZES), newPos) ||
         getColliding(sf::FloatRect(MAIN_BUTTONS_X, gui.getSize().y / HALF - sf::Vector2i(MAIN_BUTTONS_SIZES).y - MAIN_BUTTONS_MARGIN, MAIN_BUTTONS_SIZES), newPos)))
    {
        hoveredOption = mainButtonsStrings[getColliding(sf::FloatRect(MAIN_BUTTONS_X, gui.getSize().y / HALF + MAIN_BUTTONS_MARGIN, MAIN_BUTTONS_SIZES), newPos) ? 1 : 0];
        found = true;
    }
    // if the mouse is inside the param buttons' bounderies
    else if (newPos.x > PARAMS_BUTTONS_X && newPos.x < PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x)
    {
        for (i = 0; i < paramsStrings.size() && !found; i++)
        {
            // checking if there is collisions with any of the buttons
            if (getColliding(sf::FloatRect(PARAMS_BUTTONS_X, offset + (PARAMS_BUTTONS_HEIGHT + SPACE_BETWEEN_PARAMS_BUTTONS) * i, PARAMS_BUTTONS_SIZES), newPos))
            {
                hoveredOption = paramsStrings[i];
                found = true;
            }
        }
    }

    // if nothing was found to be selected, update the variable to be empty
    if (!found)
    {
        hoveredOption = "";
    }
}

// A function to update the selected param based on the pressed key
// Return if the particle was changed and needs to be reconstructed
bool GUI::updateParam(const int keyVal)
{
    int paramValue = params[selectedOption].currentValue;
    bool typeChanged = false;

    if (keyVal >= NUMBERS_ASCII_START && keyVal < NUMBERS_ASCII_END) // if the key is a number
    {
        if (clearField)
        {
            params[selectedOption].currentValue = keyVal - NUMBERS_ASCII_START;
            clearField = false;
        }
        else
        {
            params[selectedOption].currentValue = params[selectedOption].currentValue * ADD_OR_SUB_ZERO + keyVal - NUMBERS_ASCII_START;
        }

        typeChanged = checkValueMinMax();
    }
    else if (keyVal == BACKSPACE_ASCII_VALUE) // if the key is backspace
    {
        params[selectedOption].currentValue /= ADD_OR_SUB_ZERO;
        clearField = !params[selectedOption].currentValue;
        typeChanged = checkValueMinMax();
    }
    else if (keyVal == MINUS_ASCII_VALUE)
    {
        params[selectedOption].currentValue *= -1;
        typeChanged = checkValueMinMax();
    }
    return typeChanged || params[selectedOption].currentValue != paramValue;
}

// A function to check if the value of the selected option is out of bounds and fix that
// Also updates min/max values if the selected param has another boundry
bool GUI::checkValueMinMax()
{
    std::string partner;

    if (params[selectedOption].currentValue > params[selectedOption].maxValue)
    {
        params[selectedOption].currentValue = params[selectedOption].maxValue;
    }
    else if (params[selectedOption].currentValue < params[selectedOption].minValue)
    {
        params[selectedOption].currentValue = params[selectedOption].minValue;
    }

    if (selectedOption.find("Highest") != std::string::npos)
    {
        partner = selectedOption.substr(0, selectedOption.length() - CHARS_IN_HIGHEST) + "Lowest";
        params[partner].maxValue = params[selectedOption].currentValue;
        if (params[partner].currentValue > params[partner].maxValue)
        {
            params[partner].currentValue = params[partner].maxValue;
        }
    }
    else if (selectedOption.find("Lowest") != std::string::npos)
    {
        partner = selectedOption.substr(0, selectedOption.length() - CHARS_IN_LOWEST) + "Highest";
        params[partner].minValue = params[selectedOption].currentValue;
        if (params[partner].currentValue < params[partner].minValue)
        {
            params[partner].currentValue = params[partner].minValue;
        }
    }

    if (selectedOption == paramsStrings[0])
    {
        restoreParticleDefaults();
        return true;
    }
    return false;
}

void GUI::restoreParticleDefaults()
{
    switch (params[selectedOption].currentValue)
    {
    case 1:
        params = {{"Type", ParamValues{params[selectedOption].currentValue, MIN_TYPE, MAX_TYPE}}, {"Frequency", ParamValues{BUBBLES_DEFAULT_FREQUENCY, MIN_FREQUENCY, MAX_FREQUENCY}}, {"Life Span", ParamValues{BUBBLES_DEFAULT_LIFE_SPAN, MIN_LIFE_SPAN, MAX_LIFE_SPAN}}, {"Line Length", ParamValues{BUBBLES_DEFUALT_LINE_LEN, MIN_LINE_LEN, MAX_LINE_LEN}}, {"Momentum X Lowest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_X_MIN, MIN_MOMENTUM_X, BUBBLES_DEFAULT_MOMENTUM_X_MAX - 1}}, {"Momentum X Highest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_X_MAX, BUBBLES_DEFAULT_MOMENTUM_X_MIN + 1, MAX_MOMENTUM_X}}, {"Momentum Y Lowest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_Y_MIN, MIN_MOMENTUM_Y, BUBBLES_DEFAULT_MOMENTUM_Y_MAX - 1}}, {"Momentum Y Highest", ParamValues{BUBBLES_DEFAULT_MOMENTUM_Y_MAX, BUBBLES_DEFAULT_MOMENTUM_Y_MIN + 1, MAX_MOMENTUM_Y}}, {"Verticle Gravity", ParamValues{BUBBLES_DEFUALT_VERTICLE_GRAVITY, MIN_VERTICLE_GRAVITY, MAX_VERTICLE_GRAVITY}}, {"Horizontal Gravity", ParamValues{BUBBLES_DEFUALT_HORIZONTAL_GRAVITY, MIN_HORIZONTAL_GRAVITY, MAX_HORIZONTAL_GRAVITY}}};
        break;

    case 2:
        params = {{"Type", ParamValues{params[selectedOption].currentValue, MIN_TYPE, MAX_TYPE}}, {"Frequency", ParamValues{SPARKS_DEFAULT_FREQUENCY, MIN_FREQUENCY, MAX_FREQUENCY}}, {"Life Span", ParamValues{SPARKS_DEFAULT_LIFE_SPAN, MIN_LIFE_SPAN, MAX_LIFE_SPAN}}, {"Line Length", ParamValues{SPARKS_DEFUALT_LINE_LEN, MIN_LINE_LEN, MAX_LINE_LEN}}, {"Momentum X Lowest", ParamValues{SPARKS_DEFAULT_MOMENTUM_X_MIN, MIN_MOMENTUM_X, SPARKS_DEFAULT_MOMENTUM_X_MAX - 1}}, {"Momentum X Highest", ParamValues{SPARKS_DEFAULT_MOMENTUM_X_MAX, SPARKS_DEFAULT_MOMENTUM_X_MIN + 1, MAX_MOMENTUM_X}}, {"Momentum Y Lowest", ParamValues{SPARKS_DEFAULT_MOMENTUM_Y_MIN, MIN_MOMENTUM_Y, SPARKS_DEFAULT_MOMENTUM_Y_MAX - 1}}, {"Momentum Y Highest", ParamValues{SPARKS_DEFAULT_MOMENTUM_Y_MAX, SPARKS_DEFAULT_MOMENTUM_Y_MIN + 1, MAX_MOMENTUM_Y}}, {"Verticle Gravity", ParamValues{SPARKS_DEFUALT_VERTICLE_GRAVITY, MIN_VERTICLE_GRAVITY, MAX_VERTICLE_GRAVITY}}, {"Horizontal Gravity", ParamValues{SPARKS_DEFUALT_HORIZONTAL_GRAVITY, MIN_HORIZONTAL_GRAVITY, MAX_HORIZONTAL_GRAVITY}}};
        break;

    case 3:
        params = {{"Type", ParamValues{params[selectedOption].currentValue, MIN_TYPE, MAX_TYPE}}, {"Frequency", ParamValues{CONFETTI_DEFAULT_FREQUENCY, MIN_FREQUENCY, MAX_FREQUENCY}}, {"Life Span", ParamValues{CONFETTI_DEFAULT_LIFE_SPAN, MIN_LIFE_SPAN, MAX_LIFE_SPAN}}, {"Line Length", ParamValues{CONFETTI_DEFUALT_LINE_LEN, MIN_LINE_LEN, MAX_LINE_LEN}}, {"Momentum X Lowest", ParamValues{CONFETTI_DEFAULT_MOMENTUM_X_MIN, MIN_MOMENTUM_X, CONFETTI_DEFAULT_MOMENTUM_X_MAX - 1}}, {"Momentum X Highest", ParamValues{CONFETTI_DEFAULT_MOMENTUM_X_MAX, CONFETTI_DEFAULT_MOMENTUM_X_MIN + 1, MAX_MOMENTUM_X}}, {"Momentum Y Lowest", ParamValues{CONFETTI_DEFAULT_MOMENTUM_Y_MIN, MIN_MOMENTUM_Y, CONFETTI_DEFAULT_MOMENTUM_Y_MAX - 1}}, {"Momentum Y Highest", ParamValues{CONFETTI_DEFAULT_MOMENTUM_Y_MAX, CONFETTI_DEFAULT_MOMENTUM_Y_MIN + 1, MAX_MOMENTUM_Y}}, {"Verticle Gravity", ParamValues{CONFETTI_DEFUALT_VERTICLE_GRAVITY, MIN_VERTICLE_GRAVITY, MAX_VERTICLE_GRAVITY}}, {"Horizontal Gravity", ParamValues{CONFETTI_DEFUALT_HORIZONTAL_GRAVITY, MIN_HORIZONTAL_GRAVITY, MAX_HORIZONTAL_GRAVITY}}};
        break;

    default:
        break;
    }
}

// A function to update the preview particle with the new parameters
void GUI::updateParticle()
{
    sf::Vector2f currentPos = preview->getPosition();
    delete preview;
    switch (params["Type"].currentValue)
    {
    case 1:
        preview = new P_Bubbles(PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN + (gui.getSize().x - PARTICLE_WINDOW_MARGIN * DOUBLE - PARAMS_BUTTONS_X - sf::Vector2i(PARAMS_BUTTONS_SIZES).x) / HALF - params["Line Length"].currentValue / HALF, PARTICLE_WINDOW_MARGIN + gui.getSize().y / HALF, params["Line Length"].currentValue, params["Life Span"].currentValue, params["Frequency"].currentValue, params["Momentum X Lowest"].currentValue, params["Momentum X Highest"].currentValue, params["Momentum Y Lowest"].currentValue, params["Momentum Y Highest"].currentValue, params["Verticle Gravity"].currentValue, params["Horizontal Gravity"].currentValue);
        break;

    case 2:
        preview = new P_Sparks(PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN + (gui.getSize().x - PARTICLE_WINDOW_MARGIN * DOUBLE - PARAMS_BUTTONS_X - sf::Vector2i(PARAMS_BUTTONS_SIZES).x) / HALF - params["Line Length"].currentValue / HALF, PARTICLE_WINDOW_MARGIN + gui.getSize().y / HALF, params["Line Length"].currentValue, params["Life Span"].currentValue, params["Frequency"].currentValue, params["Momentum X Lowest"].currentValue, params["Momentum X Highest"].currentValue, params["Momentum Y Lowest"].currentValue, params["Momentum Y Highest"].currentValue, params["Verticle Gravity"].currentValue, params["Horizontal Gravity"].currentValue);
        break;

    case 3:
        preview = new P_Confetti(PARAMS_BUTTONS_X + sf::Vector2i(PARAMS_BUTTONS_SIZES).x + PARTICLE_WINDOW_MARGIN + (gui.getSize().x - PARTICLE_WINDOW_MARGIN * DOUBLE - PARAMS_BUTTONS_X - sf::Vector2i(PARAMS_BUTTONS_SIZES).x) / HALF - params["Line Length"].currentValue / HALF, PARTICLE_WINDOW_MARGIN + gui.getSize().y / HALF, params["Line Length"].currentValue, params["Life Span"].currentValue, params["Frequency"].currentValue, params["Momentum X Lowest"].currentValue, params["Momentum X Highest"].currentValue, params["Momentum Y Lowest"].currentValue, params["Momentum Y Highest"].currentValue, params["Verticle Gravity"].currentValue, params["Horizontal Gravity"].currentValue);
        break;

    default:
        break;
    }
}