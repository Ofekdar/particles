#include "Particle.cpp"

/*
Sparks particle class
*/
class P_Sparks : public Particle
{
private:
    void setOriginAroundRotation(sf::Transformable &object, const sf::Vector2f &newOrigin);
    void makeShape(part &newPart);
    void updateMiddleware(part &updatePart, const int delta_time);

public:
    P_Sparks(const float x, const float y, const int particleLineLen, const int lifeSpan, const int partsFrequency, const int momXMin, const int momXMax, const int momYMin, const int momYMax, const int vertGravity, const int horGravity);
};

P_Sparks::P_Sparks(const float x, const float y, const int particleLineLen = SPARKS_DEFUALT_LINE_LEN, const int lifeSpan = SPARKS_DEFAULT_LIFE_SPAN, const int partsFrequency = SPARKS_DEFAULT_FREQUENCY, const int momXMin = SPARKS_DEFAULT_MOMENTUM_X_MIN, const int momXMax = SPARKS_DEFAULT_MOMENTUM_X_MAX, const int momYMin = SPARKS_DEFAULT_MOMENTUM_Y_MIN, const int momYMax = SPARKS_DEFAULT_MOMENTUM_Y_MAX, const int vertGravity = SPARKS_DEFUALT_VERTICLE_GRAVITY, const int horGravity = SPARKS_DEFUALT_HORIZONTAL_GRAVITY) : Particle(x, y, particleLineLen, lifeSpan, partsFrequency, momXMin, momXMax, momYMin, momYMax, vertGravity, horGravity)
{
}

// function by AlexAUT (June 22, 2014) - https://en.sfml-dev.org/forums/index.php?topic=15568.0
void P_Sparks::setOriginAroundRotation(sf::Transformable &object, const sf::Vector2f &newOrigin)
{
    auto offset = newOrigin - object.getOrigin();
    object.setOrigin(newOrigin);
    object.move(offset);
}

// A function to make the shape of the sparks' particle
void P_Sparks::makeShape(part &newPart)
{
    float r = rand() % SPARKS_MAX_RADIUS_ADD + SPARKS_MIN_RADIUS;
    int x = position.x, y = position.y;

    newPart.shape = new sf::ConvexShape(POINTS_IN_SPARK);

    // making all the coords, using MATH
    (*(sf::ConvexShape *)newPart.shape).setPoint(0, sf::Vector2f(x, y - r));
    (*(sf::ConvexShape *)newPart.shape).setPoint(1, sf::Vector2f(x + r / HALF * cos(RADIANS_HALF_RADIUS), y - r / HALF * sin(RADIANS_HALF_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(2, sf::Vector2f(x + r * cos(RADIANS_FULL_RADIUS), y - r * sin(RADIANS_FULL_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(3, sf::Vector2f(x + r / HALF, y));
    (*(sf::ConvexShape *)newPart.shape).setPoint(4, sf::Vector2f(x + r * cos(RADIANS_FULL_RADIUS), y + r * sin(RADIANS_FULL_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(5, sf::Vector2f(x + r / HALF * cos(RADIANS_HALF_RADIUS), y + r / HALF * sin(RADIANS_HALF_RADIUS)));

    (*(sf::ConvexShape *)newPart.shape).setPoint(6, sf::Vector2f(x, y + r));
    (*(sf::ConvexShape *)newPart.shape).setPoint(7, sf::Vector2f(x - r / HALF * cos(RADIANS_HALF_RADIUS), y + r / HALF * sin(RADIANS_HALF_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(8, sf::Vector2f(x - r * cos(RADIANS_FULL_RADIUS), y + r * sin(RADIANS_FULL_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(9, sf::Vector2f(x - r / HALF, y));
    (*(sf::ConvexShape *)newPart.shape).setPoint(10, sf::Vector2f(x - r * cos(RADIANS_FULL_RADIUS), y - r * sin(RADIANS_FULL_RADIUS)));
    (*(sf::ConvexShape *)newPart.shape).setPoint(11, sf::Vector2f(x - r / HALF * cos(RADIANS_HALF_RADIUS), y - r / HALF * sin(RADIANS_HALF_RADIUS)));

    newPart.shape->setFillColor(sf::Color(SPARKS_COLOR));

    newPart.shape->setOutlineThickness(SPARKS_OUTLINE_THICKNESS);
    newPart.shape->setOutlineColor(sf::Color(SPARKS_OUTLINE_COLOR));
    newPart.shape->setPosition(rand() % lineLen, 0);
}

// A function to update properties specific to the sparks' particle before drawing
void P_Sparks::updateMiddleware(part &updatePart, const int delta_time)
{
    updatePart.shape->rotate(updatePart.momentum_x * SPARKS_ROTATION_SCALE * delta_time);
    setOriginAroundRotation(*updatePart.shape, position);
}