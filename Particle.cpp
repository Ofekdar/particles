#include <SFML/Graphics.hpp>
#include <functional>
#include <iostream>
#include <vector>
#include <math.h>

#include "Constantes.cpp"

#ifndef PARTICLE_CPP
#define PARTICLE_CPP

#define SELECTOR_COLOR 0, 0, 255, 80
#define SELECTOR_WIDTH 10
#define SELECTOR_HEIGHT 8

/*
A structure for a single particles' part
*/
typedef struct part
{
    sf::Shape *shape;
    float momentum_x;
    float momentum_y;
    int lifeSpan;
} part;

/*
Abstract class for different particle types
*/
class Particle
{
private:
    sf::RectangleShape selectorRect;

protected:
    std::vector<part> particles;

    sf::Vector2f position;
    int lineLen;
    int partsLifeSpan;
    int frequency; // **the lower the number, the higher the frequency!**

    int momentumXMin;
    int momentumXMax;
    int momentumYMin;
    int momentumYMax;

    int verticalGravity;
    int horizontalGravity;

    virtual void makeShape(part &newPart) = 0;
    virtual void updateMiddleware(part &updatePart, const int delta_time) = 0;

public:
    Particle(const float x, const float y, const int particleLineLen, const int lifeSpan, const int partsFrequency, const int momXMin, const int momXMax, const int momYMin, const int momYMax, const int vertGravity, const int horGravity);
    virtual ~Particle()
    {
        // deleting all created shapes
        for (auto p : particles)
        {
            delete p.shape;
        }
    }

    void move(const int delta_time);
    void blit(sf::RenderWindow &window) const;
    sf::Vector2f getPosition() const;
    void setPosition(const sf::Vector2f newPos);
};

Particle::Particle(const float x, const float y, const int particleLineLen, const int lifeSpan, const int partsFrequency, const int momXMin, const int momXMax, const int momYMin, const int momYMax, const int vertGravity, const int horGravity) : position(sf::Vector2f(x, y)),
                                                                                                                                                                                                                                                     lineLen(particleLineLen),
                                                                                                                                                                                                                                                     partsLifeSpan(lifeSpan),
                                                                                                                                                                                                                                                     frequency(partsFrequency),
                                                                                                                                                                                                                                                     momentumXMin(momXMin),
                                                                                                                                                                                                                                                     momentumXMax(momXMax),
                                                                                                                                                                                                                                                     momentumYMin(momYMin),
                                                                                                                                                                                                                                                     momentumYMax(momYMax),
                                                                                                                                                                                                                                                     verticalGravity(vertGravity),
                                                                                                                                                                                                                                                     horizontalGravity(horGravity)
{
    selectorRect = sf::RectangleShape(sf::Vector2f(lineLen + SELECTOR_WIDTH, SELECTOR_HEIGHT));
    selectorRect.setPosition(sf::Vector2f(position.x - SELECTOR_WIDTH / HALF, position.y - SELECTOR_HEIGHT / HALF));
    selectorRect.setFillColor(sf::Color(SELECTOR_COLOR));
    selectorRect.setOutlineThickness(-1);
}

void Particle::blit(sf::RenderWindow &window) const
{
    // drawing all the particles' parts onto the window
    for (auto p : particles)
    {
        window.draw(*p.shape);
    }
    window.draw(selectorRect);
}

void Particle::move(const int delta_time)
{
    // check randomly if to add another part
    if (!(rand() % frequency))
    {
        // the part itself
        part newPart;
        makeShape(newPart);

        // the momentum of the part - in milliseconds
        newPart.momentum_x = (momentumXMax == momentumXMin ? momentumXMin : (momentumXMin + rand() % (momentumXMax - momentumXMin))) / MOMENTUM_DIVIDER;
        newPart.momentum_y = (momentumYMax == momentumYMin ? momentumYMin : (momentumYMin + rand() % (momentumYMax - momentumYMin))) / MOMENTUM_DIVIDER;

        // the life span of the part - in milliseconds
        newPart.lifeSpan = partsLifeSpan;

        particles.push_back(newPart);
    }
    // update all parts
    for (int i = 0; i < particles.size(); i++)
    {
        part currentPart = particles[i];

        currentPart.shape->move(sf::Vector2f(currentPart.momentum_x * delta_time, -currentPart.momentum_y * delta_time));
        particles[i].lifeSpan -= delta_time;
        particles[i].momentum_y -= delta_time * verticalGravity / GRAVITY_TO_PIXEL;
        particles[i].momentum_x += delta_time * horizontalGravity / GRAVITY_TO_PIXEL;

        updateMiddleware(currentPart, delta_time);

        if (currentPart.lifeSpan <= 0)
        {
            delete currentPart.shape;
            particles.erase(particles.begin() + i);
        }
    }
}

// A function to get the particle's position
sf::Vector2f Particle::getPosition() const
{
    return position;
}

// A function to retrieve the particle's position
void Particle::setPosition(const sf::Vector2f newPos)
{
    position = newPos;
}

#endif