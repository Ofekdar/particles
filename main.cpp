#include <time.h>
#include <map>

#include "GUI.cpp"

int main()
{
    // initializing some starting variables
    sf::ContextSettings settings;
    settings.antialiasingLevel = 4;
    int selectedButton = 0;
    bool stillPressed = false;
    int deltaTime = 0;

    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Particles", sf::Style::Default, settings);
    GUI menu;
    window.setFramerateLimit(MAX_FRAME_RATE);
    srand(time(NULL));
    sf::Clock clock;
    std::vector<std::unique_ptr<Particle>> particles_i;

    //particles_i.push_back(std::make_unique<P_Bubbles>(140.f, 150.f));
    //particles_i.push_back(std::make_unique<P_Bubbles>(400.f, 150.f, 100));
    //particles_i.push_back(std::make_unique<P_Sparks>(140.f, 270.f));

    sf::Vertex line[] =
        {
            sf::Vertex(sf::Vector2f(300, 0)),
            sf::Vertex(sf::Vector2f(300, 400))};
    sf::Vertex line2[] =
        {
            sf::Vertex(sf::Vector2f(0, 200)),
            sf::Vertex(sf::Vector2f(600, 200))};

    while (window.isOpen() && !menu.tick(deltaTime))
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            // if user pressed the 'X' button, close the window and exit
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        // getting current time - for frame independent calculations
        deltaTime = clock.getElapsedTime().asMilliseconds();

        // moving all particles
        for (int i = 0; i < particles_i.size(); i++)
        {
            particles_i[i].get()->move(deltaTime);
        }

        // reseting the clock
        clock.restart().asMilliseconds();

        // updating the window
        window.clear();
        for (int i = 0; i < particles_i.size(); i++)
        {
            particles_i[i].get()->blit(window);
        }

        //window.draw(line, 2, sf::Lines);
        //window.draw(line2, 2, sf::Lines);
        window.display();
    }

    return 0;
}